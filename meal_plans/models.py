from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 
from django.conf import settings
from recipes.models import Recipe

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL

class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    serving_date = models.DateTimeField()
    owner = models.ForeignKey(USER_MODEL, related_name="meal_plans", on_delete=models.CASCADE,null=True)
    recipes = models.ManyToManyField(Recipe, related_name="meal_plans")
    # image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name 