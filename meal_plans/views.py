from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy 
from django.views.generic.detail import DetailView 
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from meal_plans.models import MealPlan
# Create your views here.


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    context_object_name = "meal_plans"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
    
class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "serving_date", "recipes"]

    def form_valid(self, form):
        form.instance.owner = self.request.user 
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("meal_plans_detail", kwargs={"pk": self.object.pk})


    #FUNCTION BASED VIEW
    # def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)
    # def get_queryset(self):
    #     return MealPlan.objects.filter(owner=self.request.user)

    # def form_valid(self, form):
    #     plan = form.save(commit=False)
    #     plan.owner = self.request.user 
    #     plan.save()
    #     form.save_m2m()
    #     return redirect("meal_plan_detail", pk=plan.id)


class MealPlansDetailView(UserPassesTestMixin, DetailView):
    model = MealPlan 
    template_name ="meal_plans/detail.html"
    context_object_name = "meal_plan"

    def test_func(self):
        return self.request.user == self.get_object().owner

    # def get_queryset(self):
    #     return MealPlan.objects.filter(owner=self.request.user)

class MealPlansUpdateView(UserPassesTestMixin, UpdateView):
    model = MealPlan 
    template_name ="meal_plans/edit.html"
    fields = ["name", "date", "recipes"]

    def get_success_url(self):
        return reverse_lazy("meal_plans_detail", kwargs={"pk": self.object.pk})

    def test_func(self):
        return self.request.user == self.get_object().owner

    # def get_queryset(self):
    #     return MealPlan.objects.filter(owner=self.request.user)

class MealPlansDeleteView(UserPassesTestMixin, DeleteView):
    model = MealPlan 
    template_name = "meal_plans.delete.html"
    success_url = reverse_lazy("meal_plans_list")
    context_object_name = "meal_plan"
    
    def test_func(self):
        return self.request.user == self.get_object().owner

    # def get_queryset(self):
    #     return MealPlan.objects.filter(owner=self.request.user)

