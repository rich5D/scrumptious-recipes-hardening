from django.urls import path
from django.contrib.auth import views as auth_views

from meal_plans.views import (
    MealPlansListView,
    MealPlansDetailView,
    MealPlansCreateView,
    MealPlansUpdateView,
    MealPlansDeleteView,
) 

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans_list"),
    path("create/", MealPlansCreateView.as_view(), name="meal_plans_create"),
    path("<int:pk>", MealPlansDetailView.as_view(), name="meal_plans_detail"),
    path("<int:pk>/edit/", MealPlansUpdateView.as_view(), name="meal_plans_edit"),
    path("<int:pk>/delete/", MealPlansDeleteView.as_view(), name="meal_plans_delete"),

    # path("accounts/login", auth_views.LoginView.as_view(), name="login"),
    # path("accounts/logout", auth_views.LoginView.as_view(), name="logout"),

    ]


