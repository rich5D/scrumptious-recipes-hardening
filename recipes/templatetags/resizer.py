from django import template 
register = template.Library()


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    if servings is not None and target is not None:
        try:
            ratio = int(target) / servings
            return ingredient.amount * ratio
        except ValueError:
            pass
    return ingredient.amount

register.filter(resize_to)





# def resize_to(ingredient, desired_servings):
#     servings = ingredient.recipe.servings
#     ratio = desired_servings / servings
#     resized_amount = ingredient.amount * ratio

#     return resized_amount


#------------
# from django import template

# register = template.Library()


# def resize_to(value, arg):
#     print("value:", value)
#     print("arg:", arg)
#     return "resize done"


# register.filter(resize_to)